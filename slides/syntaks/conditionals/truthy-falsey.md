<h2> Falsy og Truthy</h2>
<h3> Falsy </h3>
<ul>
    <li> null</li>
    <li> undefined</li>
    <li> '' // tom string</li>
    <li> 0</li>
    <li> false</li>
</ul>
<div class="fragment">
    <h3> Truthy </h3>
    <ul>
        <li> !falsy </li>
    </ul>
</div>