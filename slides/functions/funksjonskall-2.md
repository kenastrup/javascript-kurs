## funksjonskall

<pre><code data-trim contenteditable> 
// function form
funksjonsobjekt(parametre)

// metode form
thisObjekt.metodenavn(parametre)
thisObjekt.[metodenavn](parametre)

// konstruktør form
new funksjonsobjekt(parametre)

// apply form
funksjonsobjekt.apply(thisObjekt, [parametre])
funksjonsobjekt.bind(thisObjekt, [parametre])
</code></pre>