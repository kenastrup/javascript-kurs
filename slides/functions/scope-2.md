## Scope - råd

- Deklarer *alle* variablene øverst i funksjonen i ett <code>var</code> statement
- Deklarer alle funksjonene før du kaller de
- Du trenger ikke gjøre det, men det er lurt.
- Dont worry. JSHint/JSLint vil minne deg på dette.

<aside class="notes">
<ul>
    <li> Javascript har mekanismer som lar deg ignorere dette</li>
    <li> Men det blir rart</li>
    <li> Unngå forvirring </li>
    <li> Tjener lite på å ikke gjøre det (cohesion) </li>
</ul>
</aside>