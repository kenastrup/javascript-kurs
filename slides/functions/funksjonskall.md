## kalle en funksjon

- ( ) suffiks operatoren markerer kall
- kan ha ingen eller flere kommaseparerte parametre inni seg
- hvis kallet har for mange parametre blir det ignorert
- for få parametre, blir disse <code>undefined</code>
- ingen typesjekk på parametrene