## <code>this</code>
- <span style="color:red;"><code>this</code> er ikke som <code>this</code> i java! </span>
- <code>this</code> lar en metode vite hvilket objekt som funksjonen handler om
- <code>this</code> gjør at en funksjon kan brukes i flere objekter
- <code>this</code> er *nøkkelen* til prototype arv

<aside class="notes">
    <ul>
        <li>This er ikke som i java</li>
        <li>this er nøkkelen til gjenbruk av kode i javascript </li>
    </ul>
</aside>