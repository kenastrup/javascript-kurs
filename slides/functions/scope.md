## Scope

- JS har ikke { block } scope
- Bare function har scope
- Variabler deklarert i en funksjon er ikke synlig utenfor funksjonen.
- selv om man har indre blokker i en funksjon så er variablene synlig i hele funksjonen

<iframe class="jsbin_mindre" src="http://jsbin.com/yisibana/2/embed?js,console" />
