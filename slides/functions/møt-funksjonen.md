## Møt funksjonen

<pre><code contenteditable data-trim>
// function expression
var kvadrat = function (x) {
    return x * x;
}

// function declaration. shorthand for function expression
function kvadrat(x) {
    return x * x;
}

// function declaration med navn
var kvadrat = function kvadrat(x) {
    return x * x;
}

</code></pre>

<aside class="notes">
    <ul>
        <li>function keyword</li>
        <li>frivillig navn i variant nummer 2</li>
        <li>null eller flere parametre i parantes adskilt av komma</li>
        <li>kropp som er adskilt i krøllparantes </li>
        <li>en eller flere statements inni kroppen</li>
        <li> function declaration er shorthand for function expression </li>
    </ul>
</aside>