## funksjon form
    funksjonsobjekt(parametre)

- <code>this</code> settes til *det globale objektet* (undefined i ES5/strict)
    - da er ikke <code>this</code> spesielt nyttig
    - kløtrer til det globale objektet
    - en indre funksjon får ikke tilgang til den ytre <code>this</this>
    - enkel fiks:
        - <code> var that = this;
    - TODO: Eksempel på dette.

<aside class="notes">
    <ul>
        <li>funksjonsformen skiller seg fra metode-form ved at den ikke er prefikset med et spesifikt objekt</li>
        <li>indre hjelpefunksjoner får egen this</li>
        <li>enkel workaround: konvensjon: that = this eller self = this. that er så tilgjengelig i indre funksjon </li>
    </ul>
</aside>