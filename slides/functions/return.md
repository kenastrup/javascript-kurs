## return statement

- return *expression*;
- return;

- den siste returnerer *undefined*
- ingen return statement returnerer også undefined
- unntaket er konstruktører ( new ) som returnerer <code>this</code>