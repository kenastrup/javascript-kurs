## function statement

- lik *function expression*
- navn er obligatorisk
- funksjonene *må* deklareres på toppen i en funksjon

<pre><code data-trim contenteditable>
function min_funksjon () {}

//utvider seg til
var min_funksjon = function min_funksjon () {}

//og videre til
var min_funksjon = undefined;
min_funksjon = function min_funksjon () {};

//NB! også assignment delen hoistes
</code></pre>