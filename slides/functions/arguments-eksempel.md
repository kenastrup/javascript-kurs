## Arguments eksempel
<pre><code contenteditable data-trim>
function sum() {
    var i,
        n = arguments.length,
        total = 0;
    for (i = 0; i < arguments.length; i += 1) {
        total += arguments[i];
    }
    return total;
}
var ti = sum(1, 2, 3, 4);
</pre></code>