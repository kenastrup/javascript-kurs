## apply form
    funksjonsobjekt.apply(thisObjekt, parametre)
    funksjonsobjekt.bind(thisObjekt, parametre)

- her sender vi inn hva this skal være
- dette bruker vi til mixin/trait
- bind bruker vi når 

function add(x, y) {
'use strict';
  console.log(this);
  return x + y;
}

add(1, 1);