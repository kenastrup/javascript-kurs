
// var menneske = new Object();
// tilsvarer. De fleste anbefaler denne. Vi ignorer derfor den første
var menneske = {};

// sette properties
menneske.kjonn = 'Mann';
menneske.fornavn = 'Ken';
menneske.etternavn = 'Astrup';

// sette en function som property
menneske.fulltNavn = function () {
    return this.fornavn + ' ' + this.etternavn;
};

//tilsvarer
var menneske = {
    kjonn: 'Mann',
    fornavn: 'Ken',
    etternavn: 'Astrup'
    fulltNavn: function () {
        return this.fornavn + ' ' + this.etternavn;
    }
};

menneske
// titte litt på denne her. Sjekk __proto__ property. den er Object


menneske.fulltNavn(); // 'Ken Astrup'


// konstruktør funksjon
function Menneske( kjonn, fornavn, etternavn) {
    this.kjonn = kjonn;
    this.fornavn = fornavn;
    this.etternavn = etternavn;
};

// legger funksjonen på PROTOTYPEN til funksjonen. Den vil da gjelde ALLE instanser av Menneske,
// tilsvarer på en måte da en klasse.
Menneske.prototype.fulltNavn = function () {
    return this.fornavn + ' ' + this.etternavn;
}

var menneske2 = new Menneske('Mann', 'Ken', 'Astrup');

// sjekk nå menneske3 i konsollet
menneske3
// se at det listes ut en funksjon på objektet, men hvis vi driller ned,
//ser vi at selve objektet bare har de normale propertyene, men at
// prototypen har funksjonen.

// prøver uten NEW
var menneske3 = Menneske('Mann', 'Ken', 'Astrup');
// ser at den feiler fordi konstruktørfunksjonen ikke returner this.
// fikser metoden:
function Menneske( kjonn, fornavn, etternavn) {
    this.kjonn = kjonn;
    this.fornavn = fornavn;
    this.etternavn = etternavn;
    return this;
};
var menneske4 = Menneske('Mann', 'Ken', 'Astrup');
menneske4

// hva i huleste skjedde?
// this ble IKKE bundet til det nye objektet. men til GLOBAL (WINDOW)
// Det betyr at konstruktør funksjoner MÅ bruke NEW eller så vil dårlige ting skje.
// Det er derfor man bruker konvesjonen med Uppercase første bokstav
// DET er jo bombesikkert (ironi)
// konstruktørfunksjoner er bare normale funksjoner. Det er NEW som utfører magien

//

menneske4 instanceof Menneske; // true
menneske4 instanceof Object // true
menneske5.__proto__ // Menneske
menneske5.__proto__.__proto__ // Object


function Menneske( kjonn, fornavn, etternavn) {
    // beskytter mot glemming av new,
    if (!(this instanceof Menneske)) {
        return new Menneske(kjonn, fornavn, etternavn);
    }
    this.kjonn = kjonn;
    this.fornavn = fornavn;
    this.etternavn = etternavn;
    return this;
};
var menneske4 = Menneske('Mann', 'Ken', 'Astrup');

Menneske.prototype // Menneske, denne er immutable, men man kan fortsatt endre på prototypen!
Menneske.__proto__ // function Empty()


