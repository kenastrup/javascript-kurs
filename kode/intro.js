/*global console */

// Javascript er løst typet
var a = 'Ken';
console.log(typeof a, a);

a = 2;
console.log(typeof a, a);

a = [1, 2, 3];
console.log(typeof a, a);

a = {
    navn: 'Ken',
    adresse: 'Kristiansand'
};

console.log(typeof a, a);