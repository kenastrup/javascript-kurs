# Plan for javascript kurs

## Outline

0. Mål
  1. Nok basic javascript til å komme igang.
    1. var, const
    2. typer
    3. events
    4. dom. har dårlig api. bruk jquery
  2. Nok jQuery til å komme igang.
  3. Nok mocha til å komme igang.
  4. Nok underscore til å komme igang.
- Hvor kan man finne hjelp?
  Mozilla developer Network - https://developer.mozilla.org/en-US/docs/Web/JavaScript
1. Introduksjon
  1. Hva er javascript?
  2. Historikk og demo?
2. Byggesteinene i javascript
  1. Kommentarer og variabler
  2. Null og undefined
  3. Objekter
  4. Likhet
3. Functions
  1. "the ultimate"
  2. Møt funksjonen
  3. Kalle funksjon
  4. Arguments objektet
  5. Rekursjon
  6. Closure
4. Kontrollflyt
  1. if og switch
  2. iterereing
  3. feilhåndtering
5. Datatyper
  1. String
  2. Stringmetoder
  3. Number
  4. Array
  5. Regex
  6. Date
  7. JSON
  8. Math
  9. NaN


Gjenstående:
- nodes REPL
- repl.it
- Truthy og Falsy
- coersion
- equality
-- strict equality
- this
- prototype
- JSON
- jQuery
  - ajax
  - selectors
- functional javascript
  - moro!!!!
  -

Hopper over:
- regexp