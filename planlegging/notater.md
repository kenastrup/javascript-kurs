#Mål
ECMAScript 5.1/Strict
- dette utelukker en del browsere. Hurra.

#Intro
javascript !== java
javascript !== java!!
javascript designet og skrev en prototype som virket på 10 dager.

Begge har C-lik syntax.
Begge kjører sandboxed
Java-lik syntax var et krav når JavaScript ble utviklet (på to uker)
JavaScript Math og Date er modellert etter de samme klassene fra Java 1.0

Eich:
 If I had done classes in JavaScript back qin May 1995, I would have been told that it
 was too much like Java or that JavaScript was competing with Java... I was under marketing
 orders to make it look like Java but not make it too big for its britches... [it] needed to
 be a silly little brother language.

 Eich hadde skrevet mange programmeringsspråk tidligere. Det var ikke det vanskelige.
 Vanskelig: rikt og kraftig språk uten OO syntax som var reservert for Java.
 Han embedded avanserte features i JS uten språk-syntax, slik at det ville se enkelt
 og lettvekts ut, men fortsatt slik at sofistikerte programmerere kunne utnytte underliggende
 kraften.

 Basic syntax fra C, curly braces, semikolon, reserverte ord.
 Enklere syntax og bedre dynamisk memory håndtering.

 En webside levde typisk fra et par sekunder til et par minutter. Da kan man ta seg noen friheter
 og forenklinger iforhold til samtidighet og memory management.

 Resultat:
 Objekt model:
 - structs fra C,
 - patterns fra SmallTalk,
 - Symmetri melom data og kode fra LISP.
 - Event-modellen til Apples Hypercard inspirerte event-handling i DOM.
 - OO-patterns ble til via runtime semantics med prototypes (som i Self) istedetfor compilerte klasser som i Java og C++.


Forskjellene er større enn likhetene.
Java                JavaScript
Statisk typing      Dynamisk typing
Kompilert bytecode  As-is (vel...) JIT kompilatorer > 2012
Klassebasert        Prototype
Funksjonell java 8  Funksjonell ihvertfall fra versjon 1.2 (1997). Arvet fra Scheme



Dynamisk språk.
1. Web browsers
2. Web servere
3. Annet

client-side
1. interaksjon med bruker
2. kontrollere browseren
3. asynkron kommunikasjon med server
4. manipulere documentet (DOM) i browseren

Finnes ikke klasser som i java
Istedet Prototype-basert
Funksjoner er første-klasses medlemmer av språket.
 - kan assignes til variable
 - kan sendes som parameter til funksjoner
 - kan returneres fra funksjoner.

Multi-paradigme språk
 - OO
 - imperative
 - funksjonelt

Definert i ECMA Script (ES)

Utviklet under navnet Mocha
 - renamet til LiveScript
 - renamet til JavaScript
 - standardisert som ECMA Script

Java-lik syntax,
Design prinsippene er tatt fra Self og Scheme.

Eich:(om populariteten til JS) ...
... Yet many curse it, including me. I still think of it as a quickie love-child of C and Self. Dr. Johnson‘s words come to mind: “the part that is good is not original, and the part that is original is not good.”

##Linker
http://www.computer.org/csdl/mags/co/2012/02/mco2012020007-abs.html
http://brendaneich.com/2008/04/popularity/


#Features
## Imperativ og strukturert
- Strukturert programmering syntax kommer fra C (if, while, swich ...)
- Scoping er annerledes.
  - har ikke block-scoping
  - function scope.
  - JS 1.7 + let gir block scope
  - ASI. Automatic Semicolon Insertion. Skummelt å basere seg på dette. Bruk JSLint/JSHint for å oppdage det

 ##Dynamisk

 ###Dynamisk typing
 typer er bundet til verdier, ikke variabler.
 var x = 1;
 x = "dette er en streng";

 ###Object basert
 JavaScript objekter er assosiative arrays som utfylles av prototyper.
 Objektenes propertynavn er string keys.

 properties kan legges til, endres eller slettes i runtime.
 man kan loope over propertyene til objektene. Ikke alltid lurt men...

 Noen få innebygde Objekter:
 - Function
 - Date
 - Number
 - String

## Funksjonell
Funksjoner er første-klasses medlemmer av javascript. De er objekter.
Kan ha
- funksjoner
- properties
- funksjoner kan nestes( en funksjon kan ha en funksjon, som kan ha en funksjon ... )
- hver funksjon lager et lexical closure: det lexicale scopet til en ytre funksjon, inkludert funksjoner, variabler, properties er tilgjengelig i en indre funksjon, selv etter at funksjonen er avsluttet.

## Prototypebasert
Java objekter oppstår fra klasser
JavaScript opprettes fra prototyper, man "arver" (egentlig delegerer) properties og funksjoner til prototypen

### Funksjoner som objekt konstruktører
- new. oppretter en instans av en prototype, arver metoder og properties fra prototypen, man får også en prototype property
- Hvis man siden endrer prototypen, vil også EKSISTERENDE objekter modifiseres
NB! Eksempler

## Implisitt og Explisitt delegering
Javascript er et delegasjons språk.
### Type komposisjon og arv
Mor
  .visNavn
  Barn
  .annet

Barn.visNavn()

her delegeres funksjonskallet til Mor med Barn som this kontekst. Dette skjønner vi direkte.

### Funksjoner som Roles (Traits og mixins)
Man kopierer alle properties (og metoder) fra ett objekt til ett annet.
eksempel

Person
  extend med
    festdeltaker
    ansatt

Backbone bruker Traits overalt (.extend())


##Diverse
###Variadic functions
Man kan slenge med uendelig mange parametre til en funksjon. Funksjonen har tilgang til disse gjennom formelle parametre og også gjennom den lokale arguments objektet.

### object literals
JSON er avhengig av object literals syntaxen.
eksempel:
{
  "navn":    "Ken Are Astrup",
  "adresse": "Dvergsneskollen 1",
  "sted":    "Kristiansand"
}

### Currying
Ikke noe her?

### Regular Expressions
Også regex finnes. Sier ingenting om det.

### JavaScript